let problem1 = function (inventory, id) {
  if (
    inventory === undefined||
    inventory.constructor.name !== "Array" ||
    id === undefined
  ) {
    return null;
  } else {
    for (let car_info of inventory) {
      if (car_info["id"] === id) {
        result = `Car ${id} is a ${car_info.car_year} ${car_info.car_make} ${car_info.car_model}`;
        return result;
      }
    }
  }
};


module.exports = problem1;
