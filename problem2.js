const inventory = require("./data");

let problem2 = function (inventory) {
  if (inventory === undefined || inventory.constructor.name !== "Array") {
    return null;
  } else {
    length = inventory.length;
    last_car_info = inventory[length - 1];
    return `Last car is a ${last_car_info["car_make"]} ${last_car_info["car_model"]}`;
  }
};

module.exports = problem2;
