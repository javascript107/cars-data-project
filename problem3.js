const inventory = require("./data");

let problem3 = function (inventory) {
  if (inventory.length === 0 || inventory.constructor.name !== "Array") {
    return null;
  } else {
    let car_models = [];
    for (let car_info of inventory) {
      car_models.push(car_info["car_model"].toLowerCase());
    }
    result = car_models.sort();
    return result;
  }
};

module.exports = problem3;
