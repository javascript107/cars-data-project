const inventory = require("./data");

let problem4 = function (inventory) {
  if (inventory.length === 0 || inventory.constructor.name !== "Array") {
    return null;
  } else {
    let car_years = [];
    for (let car_info of inventory) {
      car_years.push(car_info["car_year"]);
    }
    result = car_years;
    return result;
  }
};

module.exports = problem4;
