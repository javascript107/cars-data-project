const inventory = require("./data");
const problem4 = require("./problem4");

let problem5 = function (inventory) {
  if (inventory.length === 0 || inventory.constructor.name !== "Array") {
    return null;
  } else {
    let car_years = problem4(inventory);
    let car_years_upto_2000 = [];
    let result=[]
    for (let car_info of car_years) {
      if (Number(car_info) < 2000) {
        car_years_upto_2000.push(car_info);
      }
    }
    result.push(car_years_upto_2000,car_years_upto_2000.length)    
    return result
  }
};


module.exports = problem5;
