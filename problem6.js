const inventory = require('./data')
let problem6 = function (inventory) {
  if (inventory.length === 0 || inventory.constructor.name !== "Array") {
    return null;
  } else {
    let BMWAndAudi = [];
    for (let car_info of inventory) {
      if (car_info['car_make'] === "BMW" || car_info['car_make'] === "Audi") {
        BMWAndAudi.push(car_info);
      }

    }
    let result= JSON.stringify(BMWAndAudi);
    return result
  }
};

module.exports = problem6;
